# Social Wiki

A MediaWiki project made to learn basics of MediaWiki

## Description

Social Wiki is a project made on MediaWiki which is being developed together with a youtube playlist in order to help developers to understand the functionality of MediaWiki and how it works. 

This project tells how to use MediaWiki Extension like PageForms and Cargo. Also in later versions this will include custom development of extensions for MediaWiki.